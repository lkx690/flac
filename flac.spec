%define xmms_inputdir %(xmms-config --input-plugin-dir 2>/dev/null || echo %{_libdir}/xmms/General)

Name: flac
Version: 1.3.2
Release: 12
Summary: encoder/decoder which support the Free Lossless Audio Codec
License: BSD and GPLv2+ and GFDL
Source0: http://downloads.xiph.org/releases/flac/flac-%{version}.tar.xz
URL: http://www.xiph.org/flac/

Patch6000: flac-cflags.patch
Patch6001: flac-memleak.patch
Provides: %{name}-libs
Obsoletes: %{name}-libs

BuildRequires:  gcc-c++ libogg-devel gcc automake autoconf libtool gettext-devel doxygen
BuildRequires: xmms-devel desktop-file-utils
Source1: xmms-flac.desktop

%description
FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3,
but lossless, meaning that audio is compressed in FLAC without any loss in quality.

%package devel
Summary: FLAC libraries and header files for development.
Requires: pkgconfig
Requires: flac

%description devel
FLAC libraries and header files for development.

%package -n xmms-flac
Summary: XMMS plugin needed to play FLAC files
License: GPLv2+
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils

%description -n xmms-flac
This package contains an XMMS input plugin to play FLAC audio streams. It also
provides symlinks for it to work with Beep Media Player, since plugins between
the two players are interchangeable.

%package help
Summary: help package for %{name} with man docs

%description help
document files for %{name}

%prep
%autosetup -n %{name}-%{version} -p1
%build
./autogen.sh -V

export CFLAGS="%{optflags} -funroll-loops"
%configure --enable-xmms-plugin --disable-silent-rules --disable-thorough-tests

make

%install
%make_install

desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

mv %{buildroot}%{_docdir}/flac* ./flac-doc
mkdir -p flac-doc-devel
mv flac-doc{/html/api,-devel}
rm flac-doc/FLAC.tag %{buildroot}%{_libdir}/*.la \
%{buildroot}%{xmms_inputdir}/*.la

%check
#make -C test check FLAC__TEST_LEVEL=0 &> /dev/null

%ldconfig_scriptlets libs

%post -n xmms-flac
update-desktop-database &> /dev/null || :

%postun -n xmms-flac
update-desktop-database &> /dev/null || :

%files
%doc flac-doc/* AUTHORS COPYING* README
%{_bindir}/flac
%{_bindir}/metaflac
%{_libdir}/*.so.*

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_datadir}/aclocal/*.m4

%files -n xmms-flac
%license COPYING.GPL
%{_datadir}/applications/xmms-flac.desktop
%{xmms_inputdir}/libxmms-flac.so

%files help
%{_mandir}/man1/*
%doc flac-doc-devel/*

%changelog
* Wed Jan 22 2020 lihongjiang <lihongjiang6@huawei.com> - 1.3.2-12
- ignore make check temporary

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.3.2-11
- Fix build dependence

* Fri Sep 6 2019 lihongjiang <lihongjiang6@huawei.com> - 1.3.2-10
- Adding url into spec

* Fri Sep 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.2-9
- Package init
